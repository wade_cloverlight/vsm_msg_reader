#!/usr/bin/env bash

input_dir=vsmPacketLogs
input_files="incrOrderFeed-2016-07-12.pkt instrumentSnapshot-2016-07-12.pkt orderSnapshot-2016-07-12.pkt topOfBook-2016-07-12.pkt"

output_dir=.
mkdir -p ${output_dir}

for input_file in ${input_files}; do
    output_file=${output_dir}/`basename ${input_file} .pkt`.out.gz
    if [ -e ${output_file} ]; then
        rm ${output_file}
    fi

    input_file=${input_dir}/${input_file}

    ./bld/dbg/src/vsm_msg_reader-vsm-dbg -i ${input_file} |gzip > ${output_file}
    echo ${output_file}
done

