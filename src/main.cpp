#include "VSMMktDataListener.hpp"

#include <odin/md/vsm/BookingHandler.hpp>
#include <odin/md/vsm/InstrumentStore.hpp>
#include <odin/md/vsm/MessageHandler.hpp>
#include <odin/md/vsm/MessageWriter.hpp>
#include <odin/md/vsm/Packet.hpp>
#include <odin/md/vsm/Schema.hpp>
#include <odin/md/vsm/SchemaOStream.hpp>
#include <odin/t/InstrmtDefs.hpp>
#include <odin/t/InstrmtDefsLoader.hpp>

#include <byteswap.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <cstdint>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string>
#include <vector>


int
usage(boost::program_options::options_description const & a_desc,
        std::string const & a_err = "")
{
    static const char * const App = "VSM Message Reader";
    static const char * const Version = "0.1";

    std::cerr << "\n" << App << " " << Version << "\n\n";
    if (not a_err.empty())
    {
        std::cerr << "\nProgram arguments error: " << a_err << "\n";
    }
    std::cerr << "Usage: " << a_desc << "\n";
    return 1;
}

template <typename MsgHandler>
void
read_stream(std::ifstream & input_filestream, MsgHandler & msg_handler)
{
    // Docs say that delimiter will be the 8 bytes "MAGICNUM", but it's actually "MAGICKEY".
    // Will later change to 4 bytes: 0xDE AD BE EF
    //static std::string const Magic_Bytes = "MAGICNUM";
    static std::string const Magic_Bytes = "MAGICKEY";
    //static std::string const Magic_Bytes = { '\xDE', '\xAD', '\xBE', '\xEF' };

    // Files are in binary format: magic# pkt_len pkt_data
    auto read_magic_bytes = std::make_unique<char[]>(Magic_Bytes.size());
    uint16_t pkt_len = 0;
    odin::md::vsm::Packet packet;
    std::size_t packet_count = 0;
    while (not input_filestream.eof())
    {
        input_filestream.read(read_magic_bytes.get(), Magic_Bytes.size());
        auto bytes_read = input_filestream.gcount();
        if (static_cast<std::size_t>(bytes_read) != Magic_Bytes.size())
        {
            break; // Quit on partial read.
        }
        //std::cout << "Magic: " << std::string(read_magic_bytes.get(), Magic_Bytes.size()) << std::endl;

        // Read pkt_len, which is 2 bytes and big-endian so swap the bytes via GCC's bswap_16.
        // In the future, CFE will change it to little-endian so won't have to swap.
        input_filestream.read(reinterpret_cast<char *>(&pkt_len), sizeof(pkt_len));
        bytes_read = input_filestream.gcount();
        if (static_cast<std::size_t>(bytes_read) != sizeof(pkt_len))
        {
            break; // Quit on partial read.
        }
        pkt_len = bswap_16(pkt_len);
        packet.m_size = pkt_len;
        //std::cout << "Packet Length: " << pkt_len << std::endl;

        input_filestream.read(reinterpret_cast<char *>(&packet.m_header), pkt_len);
        bytes_read = input_filestream.gcount();
        if (static_cast<std::size_t>(bytes_read) != pkt_len)
        {
            break; // Quit on partial read.
        }

        msg_handler.handle(packet);
        ++packet_count;
    }
    input_filestream.close();

    //std::cout << "Packet Count: " << packet_count << std::endl;
}

int main(int a_c, char *a_v[])
{
    std::ios_base::sync_with_stdio(false);

    boost::program_options::options_description desc("Options");
    desc.add_options()
        ("help,h", "This help message.")
        ("input,i", boost::program_options::value<std::string>(), "The input file to read. Use '-' for stdin.")
        ("book,B", boost::program_options::value<bool>()->default_value(false), "Whether to print the order book.")
        ("instruments-file,I", boost::program_options::value<std::string>()->default_value(""), "Instruments file.")
        ("instrument-ids,D", boost::program_options::value<std::string>()->default_value(""), "Comma-separated list of instrument IDs for which to print (only valid if '--book yes').")
        ;

    boost::program_options::variables_map vm;
    try
    {
        boost::program_options::store(boost::program_options::parse_command_line(a_c, a_v, desc), vm);
        boost::program_options::notify(vm);
        if (vm.count("help") || !vm.count("input"))
        {
            return usage(desc);
        } 
    }
    catch (boost::program_options::error const & ex)
    {
        return usage(desc, ex.what());
    }
    std::string const & input_filename = vm["input"].as<std::string>();
    bool const book = vm["book"].as<bool>();
    std::string const & instruments_file = vm["instruments-file"].as<std::string>();
    std::string const & instrument_ids = vm["instrument-ids"].as<std::string>();

    std::ifstream input_filestream(input_filename, std::ios::in|std::ios::binary);
    if (not input_filestream.is_open())
    {
        return 0;
    }

    if (book)
    {
        // Load instrument definitions and store them in the security store so they can be subscribed to.
        odin::t::InstrmtDefs instrument_defs;
        if (not instruments_file.empty())
        {   
            odin::t::load_instrmtdefs(instruments_file, instrument_defs);
            odin::md::vsm::InstrumentStoreSingleton::instance().store(instrument_defs);
        }

        // Print book.
        auto && instrument_store = odin::md::vsm::InstrumentStoreSingleton::instance();
        auto book_handler = std::make_shared<odin::md::vsm::BookingHandler>(instrument_store);
        auto msg_handler = std::make_shared<odin::md::vsm::MessageHandler>(book_handler, instrument_store);

        std::vector<std::string> instrument_id_str_list;
        boost::split(instrument_id_str_list, instrument_ids, boost::is_any_of(","));

        auto listener = std::make_shared<VSMMktDataListener>(std::cout);

        // Subscribe to each instrument ID.
        for (auto && instrument_id_str : instrument_id_str_list)
        {   
            if (instrument_id_str.empty())
            {   
                continue;
            }

            // Allow both a numeric ID like 1062785 or the corresponding symbol name.
            try
            {
                odin::md::vsm::InstrumentId const instrument_id{boost::lexical_cast<odin::md::vsm::InstrumentId::value_type>(instrument_id_str)};
                msg_handler->subscribe(instrument_id, listener);
            }
            catch (boost::bad_lexical_cast const &)
            {
                msg_handler->subscribe(instrument_id_str, listener);
            }
        }

        read_stream(input_filestream, *msg_handler);
    }
    else
    {
        // Print messages.
        odin::md::vsm::MessageWriter msg_handler;
        read_stream(input_filestream, msg_handler);
    }

    return 0;
}
