#include "VSMMktDataListener.hpp"

#include <odin/md/vsm/OrderBook.hpp>
#include <odin/md/vsm/Schema.hpp>
#include <odin/md/vsm/SchemaOStream.hpp>


VSMMktDataListener::
VSMMktDataListener(std::ostream & os)
    : m_os(os)
{
}

void
VSMMktDataListener::
onOrderBookUpdate(odin::md::vsm::OrderBook const & a_order_book)
{
    m_os << a_order_book << std::endl;
}

void
VSMMktDataListener::
onTrade(odin::md::vsm::LastSale const & a_last_sale)
{
    m_os <<  a_last_sale << std::endl;
}

void
VSMMktDataListener::
onSecurityTradingStatusChange(odin::md::vsm::MarketCondition const & a_market_condition)
{
    m_os << a_market_condition << std::endl;
}

