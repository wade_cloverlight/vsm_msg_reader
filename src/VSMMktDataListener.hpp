#pragma once
            
#include <odin/md/MktDataListener.hpp>
#include <odin/md/vsm/OrderBook.hpp>
#include <odin/md/vsm/Schema.hpp>
        
#include <iostream>


class VSMMktDataListener
    : public odin::md::MktDataListener
{       
public:

    VSMMktDataListener(std::ostream & os = std::cout);

    virtual void onOrderBookUpdate(odin::md::vsm::OrderBook const &) override;
    virtual void onTrade(odin::md::vsm::LastSale const &) override;
    virtual void onSecurityTradingStatusChange(odin::md::vsm::MarketCondition const &) override;

    std::ostream & os() const;

private:
    std::ostream & m_os;
};
                    
inline
std::ostream &
VSMMktDataListener::
os() const
{   
    return m_os;
}

