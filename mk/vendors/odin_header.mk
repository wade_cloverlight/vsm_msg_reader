ifndef ODIN_ROOT
	err := $(error ODIN_ROOT not set!)
endif


ifndef ODIN_INCL
	ODIN_INCL := $(ODIN_ROOT)/include
endif

ifeq ($(filter $(ODIN_INCL),$(CXX_VENDOR_INCLS)),)
	CXX_VENDOR_INCLS += $(ODIN_INCL)
endif
